FROM node:lts-alpine

WORKDIR /opt/project

COPY package*.json ./

RUN npm install
RUN npm install -g @angular/cli

COPY . .

EXPOSE 8080

CMD ng serve --host 0.0.0.0