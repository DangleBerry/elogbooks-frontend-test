import { Component, OnInit } from '@angular/core';
import { Job } from '../job';
import { JobService } from '../job.service';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {

  job!: Job;
  errorMessage: string = '';

  constructor(private jobService: JobService, private route: ActivatedRoute, private router: Router) { }

  getJob(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.jobService.getJob(id).subscribe(job => this.job = job, error => this.errorMessage = error);
  }

  deleteJob(event: Event): void {
    event.preventDefault();
    this.jobService.deleteJob(this.job.id).subscribe(response => {
      this.router.navigateByUrl('/job');
    }, error => this.errorMessage = error);
  }

  ngOnInit(): void {
    this.getJob();
  }
}
