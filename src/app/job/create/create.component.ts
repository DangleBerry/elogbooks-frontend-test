import { Component, OnInit } from '@angular/core';
import {JobService} from "../job.service";
import {Job} from "../job";
import {Router} from "@angular/router";

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  errorMessage: string = '';

  constructor(private jobService: JobService, private router: Router) { }

  ngOnInit(): void {
  }

  addJob(job: Job): void {
    this.jobService.addJob(job).subscribe(job => {
      this.router.navigateByUrl('/job');
    }, error => this.errorMessage = error)
  }
}
