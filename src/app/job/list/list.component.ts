import { Component, OnInit } from '@angular/core';
import { Job } from '../job';
import {JobService} from "../job.service";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  jobs: Job[] = [];
  errorMessage: string = '';

  constructor(private jobService: JobService) {}

  getJobs(): void {
    this.jobService.getJobs().subscribe(jobs => (this.jobs = jobs), error => this.errorMessage = error);
  }

  findJobIndexById(id: number): number|null {
    let result = null;
    this.jobs.forEach((job, index) => {
      if (job.id === id) {
        result = index;
      }
    });

    return result;
  }

  deleteJob(event: Event, id: number): void {
    event.preventDefault();
    this.jobService.deleteJob(id).subscribe(response => {
      let index = this.findJobIndexById(id);
      if (typeof index === "number") {
        this.jobs.splice(index, 1);
      }
    }, error => this.errorMessage = error);
  }

  ngOnInit(): void {
    this.getJobs();
  }
}
