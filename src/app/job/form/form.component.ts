import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Job} from "../job";


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  @Input() job!: Job;
  @Output() onSubmit: EventEmitter<Job> = new EventEmitter();
  CREATE_TITLE: string = "Create Job";
  EDIT_TITLE: string = "Edit Job";
  statuses: string[] = [
      'open',
      'in progress',
      'cancelled',
      'completed'
  ];

  jobForm = new FormGroup({
    id: new FormControl(''),
    summary: new FormControl('', [
      Validators.required,
      Validators.maxLength(150)
    ]),
    description: new FormControl('', [
      Validators.required,
      Validators.maxLength(500)
    ]),
    status: new FormControl('open', [
      Validators.required
    ]),
    property: new FormGroup({
      name: new FormControl('', [
        Validators.required,
        Validators.maxLength(255)
      ])
    }),
    raisedBy: new FormGroup({
      firstname: new FormControl('', [
        Validators.required,
        Validators.maxLength(32)
      ]),
      surname: new FormControl('', [
        Validators.required,
        Validators.maxLength(32)
      ])
    })
  });

  constructor() {}

  ngOnInit(): void {
    if (this.job) {
      this.jobForm.patchValue(this.job);
    } else {
      this.jobForm.removeControl('status');
    }
  }

  submit() {
    if (this.jobForm.valid) {
      this.onSubmit.emit(<Job>this.jobForm.value);
    }
  }

  get summary() { return this.jobForm.get('summary')!; }

  get description() { return this.jobForm.get('description')!; }

  get status() { return this.jobForm.get('status')!; }

  get name() { return this.jobForm.get('property.name')!; }

  get raisedByFirstname() { return this.jobForm.get('raisedBy.firstname')!; }

  get raisedBySurname() { return this.jobForm.get('raisedBy.surname')!; }
}
