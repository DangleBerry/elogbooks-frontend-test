import { Property } from './property';
import {User} from "./user";

export interface Job {
  id: number;
  summary: string;
  description: string;
  status: string;
  property: Property;
  raisedBy: User;
}
