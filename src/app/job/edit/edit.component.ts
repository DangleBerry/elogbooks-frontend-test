import { Component, OnInit } from '@angular/core';
import {Job} from "../job";
import {JobService} from "../job.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  job!: Job;
  errorMessage: string = '';

  constructor(private jobService: JobService, private route: ActivatedRoute, private router: Router) { }

  getJob(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.jobService.getJob(id).subscribe(job => this.job = job, error => this.errorMessage = error);
  }

  submit(job: Job) {
    this.jobService.editJob(job).subscribe(job => {
      this.router.navigateByUrl('/job');
    },error => this.errorMessage = error);
  }

  ngOnInit(): void {
    this.getJob();
  }

}
