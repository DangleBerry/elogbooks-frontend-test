import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { ListComponent } from './job/list/list.component';
import { CreateComponent } from './job/create/create.component';
import { EditComponent } from './job/edit/edit.component';
import { RouterModule } from '@angular/router';
import { ViewComponent } from './job/view/view.component';
import { PageNotFoundComponent } from './error/page-not-found/page-not-found.component';
import { FormComponent } from './job/form/form.component';
import {ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    CreateComponent,
    EditComponent,
    ViewComponent,
    PageNotFoundComponent,
    FormComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot([
      {path: '', redirectTo: 'job', pathMatch: 'full'},
      {
        path: 'job',
        children: [
          {path: '', component: ListComponent},
          {path: 'create', component: CreateComponent},
          {path: 'edit/:id', component: EditComponent},
          {path: 'view/:id', component: ViewComponent}
        ]
      },
      {path: '**', component: PageNotFoundComponent}
    ]),
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
